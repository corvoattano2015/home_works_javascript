function Human(firstName, secondName, age) {
  this.firstName = firstName;
  this.secondName = secondName;
  this.age = age;
}

function getProp(array, sorted) {
  for (let i = 0; i < 5; i++) {
    let props = [];
    let j = array[i];
    for (key in j) {
      props.push(j[key]);
    }
    sorted.push(props);
  }
}

function pushProps(array, constructor) {
  let arr_1 = [];
  for (let i = 0; i < array.length; i++) {
    let person_1 = new constructor(array[i][0], array[i][1], array[i][2]);
    arr_1.push({ person_1 });
  }
  return arr_1;
}

let person_1 = new Human("Oleg", "Petrov", 56);
let person_2 = new Human("Igor", "Rahmanov", 11);
let person_3 = new Human("Ivan", "Sokolov", 84);
let person_4 = new Human("Svetlana", "Ivanova", 23);
let person_5 = new Human("Dimitri", "Koperhand", 45);

let arr = [person_1, person_2, person_3, person_4, person_5];
let sortedArr = [];

getProp(arr, sortedArr);

sortedArr.sort(function (a, b) {
  return a[2] - b[2];
});

sortedArr = pushProps(sortedArr, Human);

console.log(sortedArr);

//=========================================================================================================//

let human2 = new Object(Human)
human2.getProp = function () {
  for (key in human2) {
    console.log(human2[key])
  }
}
human2.getProp()