const display = document.querySelector(".display > input");
const container = document.querySelector(".display");
const btn = document.querySelector(".keys");
const dot = document.querySelector(".dot");
let a = "";
let b = "";
let sign = "";
let finish = false;
let nums = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "."];
let action = ["+", "-", "*", "/"];
let res;
let mAdd;

function clearAll() {
  a = "";
  b = "";
  sign = "";
  finish = false;
  display.value = "0";
}
function disabled() {
  dot.style.pointerEvents = "none";
}
function enabled() {
  dot.style.pointerEvents = "auto";
}
btn.addEventListener("click", (event) => {
  let button = event.target.value;
  if (event.target.tagName !== "INPUT") return;
  if (nums.includes(button)) {
    if (b === "" && sign === "") {
      a += button;
      display.value = a;
      if (button == ".") {
        let x = [a];
        if (x[0].split(".").length > 1) {
          disabled();
        }
      }
    } else if (a !== "" && b !== "" && finish) {
      b = button;
      finish = false;
      display.value = a;
    } else {
      b += button;
      display.value = b;
      if (button == ".") {
        let x = [b];
        if (x[0].split(".").length > 1) {
          disabled();
        }
      }
    }
    return;
  }
  if (action.includes(button)) {
    sign = button;
    display.value = sign;
    if (sign != "") {
      enabled();
    }
    return;
  }
  if (button == "=") {
    switch (sign) {
      case "+":
        res = +a + +b;
        break;
      case "-":
        res = a - b;
        break;
      case "*":
        res = a * b;
        break;
      case "/":
        if (b == "0") {
          display.value = "Error";
          a = "";
          b = "";
          sign = "";
          return;
        }
        res = a / b;
        break;
    }
    finish = true;
    display.value = res;
    enabled();
  }

  if (button == "m+") {
    mAdd = display.value;
    let mSign = document.createElement("div");
    container.append(mSign);
    mSign.innerHTML = "M+";
    mSign.classList.add("mAdd");
    mSign.style.position = "absolute";
    mSign.style.left = "10px";
    mSign.style.top = "50%";
    setTimeout(() => {
      document.querySelector(".mAdd").remove();
    }, 1500);
  } else if (button == "m-") {
    mAdd = "0";
    let mSign = document.createElement("div");
    container.append(mSign);
    mSign.innerHTML = "M-";
    mSign.classList.add("mRem");
    mSign.style.position = "absolute";
    mSign.style.left = "10px";
    mSign.style.top = "50%";
    setTimeout(() => {
      document.querySelector(".mRem").remove();
    }, 1500);
  } else if (button == "mrc") {
    if (mAdd == undefined) {
      display.value = "Error";
    } else {
      display.value = mAdd;
      a = mAdd;
      let mSign = document.createElement("div");
      container.append(mSign);
      mSign.innerHTML = "MRC";
      mSign.classList.add("mShow");
      mSign.style.position = "absolute";
      mSign.style.left = "10px";
      mSign.style.top = "50%";
      setTimeout(() => {
        document.querySelector(".mShow").remove();
      }, 1500);
    }
  }
  if (button == "C") {
    clearAll();
    enabled();
  }
});
