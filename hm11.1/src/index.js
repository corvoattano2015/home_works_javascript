class User {
  constructor(name, surname) {
    this.name = name;
    this.surname = surname;
  }
  welcome() {
    console.log(`Привіт , ${this.name} ${this.surname} !`);
  }
}
let usr_name = prompt("Введіть ім'я");
let usr_surname = prompt("Введіть прізвище");

let user = new User(usr_name, usr_surname);
user.welcome();

//====================================================//

let body = document.querySelector("body"),
  list = document.createElement("ul");
body.prepend(list);

let li1 = document.createElement("li");
let li2 = document.createElement("li");
let li3 = document.createElement("li");
let li4 = document.createElement("li");

li1.innerText = "1";
li2.innerText = "2";
li3.innerText = "3";
li4.innerText = "4";

list.append(li1);
list.append(li2);
list.append(li3);
list.append(li4);

let el = document.querySelector("ul").childNodes[1];
el.previousSibling.style.background = "aqua";
el.nextSibling.style.background = "red";

//==============================================================//

let mouseEvent = document.querySelector(".mouse");
mouseEvent.addEventListener("mousemove", (e) => {
  let targetCoords = e.target.closest("div").getBoundingClientRect();
  let xCoord = e.clientX - targetCoords.left;
  let yCoord = e.clientY - targetCoords.top;
  document.querySelector("#xCoord").value = xCoord;
  document.querySelector("#yCoord").value = yCoord;
});

//==============================================================//

document.querySelector(".buttons").addEventListener("click", (e) => {
  if (e.target.tagName != "BUTTON") return;
  alert(`Натиснута кнопка ${e.target.innerText}`);
});

//==============================================================//

let width = window.getComputedStyle(document.querySelector(".sliding-container")).width.replace("px", "");
document.querySelector(".sliding-container").addEventListener("mouseover", () => {
    document.querySelector(".sliding-div").style.transform = `translateX(${Number(width) - 102}px)`;
  });
document.querySelector(".sliding-container").addEventListener("mouseout", () => {
    document.querySelector(".sliding-div").style.transform = "translateX(0px)";
  });

//==============================================================//
document.querySelector("#color-picker").addEventListener("change", (e) => {
  document.body.style.background = e.target.value;
});

//==============================================================//

document.querySelector("#login").addEventListener("keyup", (e) => {
  console.log(e.target.value);
});

//==============================================================//

document.querySelector("#txt").addEventListener("keyup", () => {
  document.querySelector(".container").textContent = document.querySelector("#txt").value;
});
