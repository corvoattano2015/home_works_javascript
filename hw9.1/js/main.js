//-----------------------Stopwatch----------------------------//
//Buttons
let start = document.querySelector("#start");
let stop = document.querySelector("#pause");
let reset = document.querySelector("#reset");
//Numbers
let display = document.querySelector(".stopwatch__display");
let ms = document.querySelector(".ms");
let sec = document.querySelector(".sec");
let min = document.querySelector(".min");
//Variables
let interval;
let milisecs = 00,
  seconds = 00,
  minutes = 00;
//Icons
let startIcon = document.querySelector(".start-icon");
let pauseIcon = document.querySelector(".pause-icon");
let restartIcon = document.querySelector(".restart-icon");

//Stopwatch
function stopWatch() {
  milisecs++;
  if (milisecs < 9) {
    ms.innerHTML = "0" + milisecs;
  }
  if (milisecs > 9) {
    ms.innerHTML = milisecs;
  }
  if (milisecs >= 99) {
    seconds++;
    sec.innerHTML = "0" + seconds;
    milisecs = 0;
  }
  if (seconds < 9) {
    sec.innerHTML = "0" + seconds;
  }
  if (seconds > 9) {
    sec.innerHTML = seconds;
  }
  if (seconds > 59) {
    minutes++;
    min.innerHTML = "0" + minutes;
    seconds = 0;
  }
  if (minutes < 9) {
    min.innerHTML = "0" + minutes;
  }
  if (minutes > 9) {
    min.innerHTML = minutes;
  }
  if (minutes >= 10) {
    minutes = "nn";
    seconds = "nn";
    milisecs = "nn";
    min.innerHTML = minutes;
    sec.innerHTML = seconds;
    ms.innerHTML = milisecs;
    window.open("https://www.youtube.com/shorts/MLVuLlbdkQY", "_blank");
  }
}
//Start button
start.addEventListener("click", () => {
  display.classList.remove("default");
  display.classList.remove("red");
  display.classList.remove("gray");
  display.classList.add("green");
  startIcon.classList.remove("invisible");
  pauseIcon.classList.add("invisible");
  restartIcon.classList.add("invisible");
  clearInterval(interval);
  interval = setInterval(stopWatch, 10);
});
//Pause button
stop.addEventListener("click", () => {
  display.classList.remove("default");
  display.classList.remove("green");
  display.classList.remove("gray");
  display.classList.add("red");
  startIcon.classList.add("invisible");
  pauseIcon.classList.remove("invisible");
  restartIcon.classList.add("invisible");
  clearInterval(interval);
});
//Reset button
reset.addEventListener("click", () => {
  display.classList.remove("default");
  display.classList.remove("green");
  display.classList.remove("red");
  display.classList.add("gray");
  startIcon.classList.add("invisible");
  pauseIcon.classList.add("invisible");
  restartIcon.classList.remove("invisible");
  clearInterval(interval);
  milisecs = 00;
  seconds = 00;
  minutes = 00;
  ms.innerHTML = "00";
  sec.innerHTML = "00";
  min.innerHTML = "00";
});

//-----------------------Checker----------------------------//

let form = document.querySelector("input");
let btnCheck = document.querySelector("#check");
let container = document.querySelector(".phone-check__container");
let checker = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){12}(\s*)?$/;
btnCheck.onclick = () => {
  if (checker.test(form.value)) {
    form.value = "Correct ! :)";
    container.classList.add("valid");
    setTimeout('container.classList.remove("valid")', 5000);
    setTimeout('form.value = ""',5000);
    setTimeout('window.open("bublik.html", "_blank")', 3000);
  } else {
    form.value = "Incorrect ,try again ! :(";
    container.classList.add("invalid");
    setTimeout('container.classList.remove("invalid")', 5000);
    setTimeout('form.value = ""', 5000);
  }
};

//------------------------Slider----------------------------//

let sliderLine = document.querySelector(".slider__line");
let position = 0;
let timer 
function nextSlide() {
  position = position + 736;
  if (position > 2944) {
    position = 0;
  }
  sliderLine.style.left = -position + "px";
}
function autoPlay() {
  timer = setInterval(nextSlide, 8000);
}

document.querySelector(".prev").addEventListener("click", () => {
  position = position - 736;
  if (position < 0) {
    position = 2944;
  }
  sliderLine.style.left = -position + "px";
  clearInterval(timer)
  timer = setInterval(nextSlide, 8000);
});

document.querySelector(".next").addEventListener("click", () => {
  position = position + 736;
  if (position > 2944) {
    position = 0;
  }
  sliderLine.style.left = -position + "px";
  clearInterval(timer);
  timer = setInterval(nextSlide, 8000);
});

autoPlay(nextSlide());

//--------------------------------------------------------//