let body = document.querySelector("body");
let button = document.querySelector("button");
let input = document.createElement("input");
let container = document.createElement("div");

button.addEventListener("click", function () {
  body.append(input);
  input.placeholder = "Введіть діаметр круга";
  button.textContent = "Намалювати круг";
});
button.addEventListener("click", function () {
  let diam = document.querySelector("input").value;
  body.append(container);
  for (let i = 0; i < 100; i++) {
    let circle = document.createElement("p");
    circle.style.display = "inline-block";
    circle.style.backgroundColor = `hsl(${Math.floor(
      Math.random() * 365
    )}, 50%, 50%)`;
    circle.style.width = `${diam}px`;
    circle.style.height = `${diam}px`;
    circle.style.borderRadius = "50%";
    circle.style.margin = "3px";

    container.prepend(circle);
  }
  for (let x = 0; x < container.children.length; x++) {
    container.children[x].onclick = function () {
      this.parentNode.removeChild(this);
    };
  }
});
