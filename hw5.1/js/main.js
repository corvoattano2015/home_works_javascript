/* Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". Создать в объекте вложенный объект - "Приложение". Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". Создать методы для заполнения и отображения документа.*/

const objDoc = {
  header: "",
  body: "",
  footer: "",
  date: "",
  app: {
    header: "",
    body: "",
    footer: "",
    date: "",
  },
  functions: {
    getUserData: function () {
      return prompt("Введите данные для заполнения");
    },
    showDoc: function (documentProp, key) {
      document.write(key + " : " + documentProp + "</br>");
    },
    fillInCycle: function (data, write, obj) {
      for (key in obj) {
        if (obj[key] == obj.app) {
          break;
        }
        let keyValue = key;
        obj[key] = data();
        write(obj[key], keyValue);
      }
    },
  },
};
objDoc.functions.fillInCycle(
  objDoc.functions.getUserData,
  objDoc.functions.showDoc,
  objDoc
);

objDoc.functions.fillInCycle(
  objDoc.functions.getUserData,
  objDoc.functions.showDoc,
  objDoc.app
);

