let arr = [420, 69, 13, 57, 90, 448, 666, 274, 999];

function arrSort(arr) {
  return arr.sort(function (a, b) {
    return a - b;
  });
}

function map(fn, array) {
  let sortedArray = [];
  for (let i = 0; i < array.length; i++) {
    sortedArray.push(array[i]);
  }
  return fn(sortedArray);
}

let sortedArray = map(arrSort, arr);
document.write("Масив до сортування - " + arr + "<br>");
document.write("Масив після сортування - " + sortedArray + "<br>");

// Перепишите функцию, используя оператор '?' или '||'
// Следующая функция возвращает true, если параметр age больше 18. В ином случае она задаёт вопрос confirm и возвращает его результат.
// function checkAge(age) {
// 2	if (age > 18) {
// 3	return true;
// 4	} else {
// 5	return confirm('Родители разрешили?');
// 6	} }

function getAge() {
  let age = parseInt(prompt("Ваш возраст?"));
  let checker = confirm("Родители разрешили?");
  age >= 18 || checker == true ? alert("Проходи") : alert("Учи уроки");
}

getAge();
