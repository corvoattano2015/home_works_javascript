
//======================================='Конструктор'====================================================//

function createNewUser(usr_name, usr_surname, bday) {
  this.name = usr_name;
  this.surname = usr_surname;
  this.bday = bday;
}

createNewUser.prototype.getAge = function () {
  let usr_bday = this.bday.split(".");
  let today_date = new Date();
  let usr_age = today_date.getFullYear() - usr_bday[2];
  console.log(usr_age);
  return usr_bday[2];
};

createNewUser.prototype.getLogin = function () {
  console.log("Ваш логін : " + (this.name[0] + this.surname).toLowerCase());
};

createNewUser.prototype.getPassword = function (year) {
  console.log(
    "Ваш пароль: " +
      this.name[0].toUpperCase() +
      this.surname.toLowerCase() +
      year
  );
};

let newUser = new createNewUser(
  prompt("Ваше ім'я?"),
  prompt("Ваше прізвище?"),
  prompt("Дата народження в форматі dd.mm.yyyy")
);
let usr_age = newUser.getAge();
newUser.getLogin();
newUser.getPassword(usr_age);

//======================================='filter'====================================================//

const arr = ["hello", "world", 23, "23", null];
function filterBy(arr, type) {
  return arr.filter(function (item) {
    return typeof item !== type;
  });
}
console.log(filterBy(arr, "string"));
console.log(filterBy(arr, "number"));
