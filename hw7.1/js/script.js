let words = [
  "красный",
  "оранжевый",
  "желтый",
  "зеленый",
  "голубой",
  "синий",
  "фиолетовый",
];

let empty = [];
let randomWord = getRandomWord(words);

function getRandomWord(arr) {
  let random = Math.floor(Math.random() * arr.length);
  return arr[random];
}

function getLetter() {
  let userLetter = prompt("Ваша буква: ");
  if (userLetter.length > 1) {
    alert("Введите одну букву");
    return 98237827193876873;
  } else if (isNaN(userLetter)) {
    return userLetter;
  } else if (!isNaN(userLetter)) {
    alert("Букву вводи,баран");
    return 98237827193876873;
  }
}

function pushEmpty(word, arr) {
  for (let i = 0; i < word.length; i++) {
    arr.push("_");
  }
  return arr;
}

function runGame(word, arr, letter1) {
  let count = word.length;
  let attempt = 10;
  while (count > 0) {
    let letter = letter1();
    if (letter == null) {
      break;
    } else if (letter == 98237827193876873) {
      continue;
    }
    for (let i = 0; i <= word.length; i++) {
      if (letter == word[i]) {
        if (arr[i] == letter) {
          continue;
        } else {
          arr[i] = letter;
          alert(arr);
          count--;
          break;
        }
      } else if (i == word.length) {
        alert("Не угадал , осталось " + attempt + " попыток");
        attempt--;
      }
    }
    if (attempt == 0 && count > 0) {
      alert("Ты проиграл");
      break;
    } else if (attempt >= 0 && count <= 0) {
      attempt = 10 - attempt;
      alert("Победа , ушло " + attempt + " попыток");
    }
    console.log(arr);
  }
}

pushEmpty(randomWord, empty);
runGame(randomWord, empty, getLetter);
